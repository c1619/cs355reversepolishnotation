import Data.List ()
import Data.Char ()
import Debug.Trace ()

-- performs the operation identified by the string. for example, doOp state "+"
-- will perform the "+" operation, meaning that it will pop two values, sum them,
-- and push the result. 
-- YOU NEED TO PROVIDE THE OPERATIONS YOURSELF (doAdd, doSub, etc.)
doOp :: String -> [Float] -> [Float]
-- here's how we turn the strings into their corresponding operation. 
doOp "+" = doAdd -- provide doAdd yourself
-- note: we could have written doOp "+" stack = doAdd stack, but we used point-free notation 
doOp "-" = doSub -- provide doSub yourself, etc.
doOp "*" = doMul
doOp "/" = doDiv 

doOp "swap" = doSwap 
doOp "drop" = doDrop 
doOp "rot" = doRot 
doOp "dup" = doDup 


-- if we go through all the definitions without finding our operation, 
-- it's not supported 
doOp op = error $ "unrecognized operation: " ++ op 


-- the "do" functions (doAdd, doSub, etc.) will have type [Float] -> [Float]. That is
-- they take a stack and return a modified stack. 
-- the operation functions are here
doAdd :: [Float] -> [Float]
doAdd [] = []
doAdd (top:second:rest) = (top + second) : rest

doSub :: [Float] -> [Float]
doSub [] = []
doSub (top:second:rest) = (second - top) : rest

doMul :: [Float] -> [Float]
doMul [] = []
doMul (top:second:rest) = (top * second) : rest

doDiv :: [Float] -> [Float]
doDiv [] = []
doDiv (top:second:rest) = (second / top) : rest

doSwap:: [Float] -> [Float]
doSwap [] = []
doSwap (top:second:rest) = second:top:rest

doDrop :: [Float] -> [Float]
doDrop [] = []
doDrop (top:second:third:rest) = second:third:rest

doRot :: [Float] -> [Float]
doRot [] = []
doRot (top:second:third:rest) = second:third:top:rest

-- duplicate the top value on the stack. 1 -> 1 1 
doDup :: [Float] -> [Float]
doDup [] = []
doDup ( top : rest ) = top : top : rest 

-- Takes a whole program and turns it into a list of tokens. 
-- Given that our tokens are just individual words separated by whitespace, we can use the
-- "words" function
tokenize :: String -> [String]
tokenize = words

-- removes comments from a token stream. comments are between /' and '/. 
-- comments may not be nested. 
-- arguments:
--  * the first bool tells us whether we are in a comment or not. starts false.
--  * the first token list is the tokens that are not inside of comments. starts empty.
--  * the last list are the remaining tokens 
-- returns: all of the tokens that are not inside of comments. 
removeComments :: Bool -> [String] -> [String] -> [String]

-- if the first argument is 'true', we're inside a comment. but the [] means no more tokens.
removeComments True _ [] = error "ended comment while it's still open. need closing '/ ."  

-- if we finish all the tokens and are not in a comment, there's nothing else to do
-- except reversing the nonComments tokens (because we've been appending to the front)
removeComments False nonComments [] = reverse nonComments

-- if we're in a comment and we find '/, we close the comment and continue 
removeComments True nonComments ( "'/":tail ) = removeComments False nonComments tail

-- if we're in a comment, ignore whatever token comes next 
removeComments True nonComments ( _:tail ) = removeComments True nonComments tail

-- if we're not in a comment and we find /', start the comment 
removeComments False nonComments ( "/'":tail ) = removeComments True nonComments tail

-- if we're not in a comment, add the token to the nonComment tokens 
removeComments False nonComments ( head:tail ) = removeComments False (head:nonComments) tail


-- takes the given tokens and a state. returns the resulting state. 
runCode :: [String] -> [Float] -> [Float]
-- running the empty program does nothing 
runCode [] state = state 

-- encountering add operation
runCode ("+":tokens) stack = runCode tokens (doOp "+" stack)

-- encountering subtract operation
runCode ("-":tokens) stack = runCode tokens (doOp "-" stack)

-- encountering multiply operation
runCode ("*":tokens) stack = runCode tokens (doOp "*" stack)

-- encountering divide operation
runCode ("/":tokens) stack = runCode tokens (doOp "/" stack)

-- encountering swap operation
runCode ("swap":tokens) stack = runCode tokens (doOp "swap" stack)

-- encountering drop operation
runCode ("drop":tokens) stack = runCode tokens (doOp "drop" stack)

-- encountering rotate operation
runCode ("rot":tokens) stack = runCode tokens (doOp "rot" stack)

-- encountering duplicate operation
runCode ("dup":tokens) stack = runCode tokens (doOp "dup" stack)

-- encountering a value should push the value 
runCode (token : tokens) stack = runCode (tokens) ((read token):stack)

main :: IO ()
main = do
    -- get all the code passed to STDIN as a giant string 
    code <- getContents

    -- convert it into a list of tokens
    let tokens = removeComments False [] ( tokenize code )  

    -- create a new stack state 
    let state = [] :: [Float] 

    -- run the code's tokens on the new state 
    -- YOU MUST DEFINE runCode
    let result = runCode tokens state 

    -- print the result. Make sure to reverse it.  
    putStrLn ""
    print $ reverse result 